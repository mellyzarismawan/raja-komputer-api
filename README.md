# README #

Raja Komputer API for complete final test of course `Distributed System`

Created By :
- Name : Mellyza Rismawan
- NPM : 14187011
- Information System - C (semester 6 / 2021)

## Getting started
This project requires
- PHP
- Composer
- Postman App (for test API)

## Setup
1. Configure api configuration file
```
cp params/.env.example params/.env
```
Don't forget to change other config such as host or port
```
DB_HOST=somehost
```
Run composer install & update
```
composer install
....
composer update
```

2. Run migration & seeder
```
php artisan migrate
```
```
php artisan db:seed --force
```

3. Run API

- Run in terminal :
```
php -S localhost:8111 -t public/
```
- Import Postman Collection That have file named `Raja Komputer.postman_collection.json`. Which is already in this folder.
- Don't forget to hit `Login` endpoint (using credentials that already available in `users` table) to get token for hit another endpoint.

- Don't forget, another endpoint (except login) is required to use `Authorization` (Bearer Token) in header.

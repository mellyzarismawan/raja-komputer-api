<?php namespace App\Requests;

class UserRequest extends Request
{
    /**
     * Check for valid PIN
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'status' => 'required',
            'type' => 'required'
        ];
        return $rules;
    }
}

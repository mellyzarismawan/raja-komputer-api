<?php

namespace App\Modules\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Credit;
use App\Models\Customer;
use App\Models\Item;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Sales;
use App\Models\Transaction;
use App\Models\TransactionItem;
use App\Models\UserBalances;
use Carbon\Traits\Date;
use Helpers\Api\ApiResponse;
use Helpers\Api\HttpResponse;
use Helpers\Paginate;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Carbon\Carbon;
use Mapping\TransactionMapping;

class TransactionsController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ApiResponse;
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|numeric',
            'qty' => 'required|numeric',
            'credit_id' => 'numeric',
            'customer_name' => 'required',
            'customer_email' => 'required|email',
            'customer_address' => 'required',
            'customer_phone' => 'required',
            'customer_gender' => 'required',
            'customer_identity_number' => 'required',
            'payment_type' => 'required',
        ]);

        // check product data
        $product = Product::where('id', $request->input('product_id'))->where('status', Product::STATUS_ACTIVE)->first();
        if (empty($product)) {
            $this->response->setMessage('Product tidak ditemukan.');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        // check credit data
        if ($request->input('payment_type') == Sales::PAYMENT_TYPE_CREDIT) {
            $credit = Credit::where('id', $request->input('credit_id'))->first();
            if (empty($credit)) {
                $this->response->setMessage('Detail credit tidak ditemukan.');
                $this->response->setToken(Auth::user()->token);
                $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
                return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
            }
        }

        // check customer data
        $customer_id = 0;
        $data_customer = Customer::where('email', $request->input('customer_email'))->first();
        if (empty($data_customer)) {
            $req_create_customer = [
                'name' => $request->input('customer_name'),
                'email' => $request->input('customer_email'),
                'address' => $request->input('customer_address'),
                'phone' => $request->input('customer_phone'),
                'gender' => $request->input('customer_gender'),
                'identity_number' => $request->input('customer_identity_number'),
                'status' => Customer::STATUS_ACTIVE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
            $result = Customer::create($req_create_customer);
            $customer_id = $result->id;
        } else {
            $customer_id = $data_customer->id;
        }

        // count total sales
        $bunga = 0;
        $lamaCredit = 0;
        if (!empty($credit)) {
            $bunga = $credit->bunga;
            $lamaCredit = $credit->lama;
        }
        $totalTmp = $product->price * $request->input('qty');
        $bungaAkhir = 0;
        if ($bunga > 0) {
            $bungaAkhir = $totalTmp * $bunga / 100;
        }
        $total = round($totalTmp + $bungaAkhir);

        // save to sales
        $req_create_sales = [
            'product_id' => $product->id,
            'credit_id' => $request->input('credit_id'),
            'customer_id' => $customer_id,
            'user_id' => Auth::user()->id,
            'qty' => $request->input('qty'),
            'bunga' => $bunga,
            'lama' => $lamaCredit,
            'total' => $total,
            'payment_type' => $request->input('payment_type'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
        $resultSalesCreate = Sales::create($req_create_sales);
        $sales_id = $resultSalesCreate->id;

        // create payment if payment type cash
        if ($request->input('payment_type') == Sales::PAYMENT_TYPE_CASH) {
            $req_create_payment = [
                'sales_id' => $sales_id,
                'amount' => $total,
                'payment_method' => $request->input('payment_method'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            Payment::create($req_create_payment);
        }

        // mapping response
        $biaya_perbulan = 0;
        if ($lamaCredit > 0) {
            $biaya_perbulan = round($total / $lamaCredit);
        }
        $data = [
            'admin_name' => Auth::user()->name,
            'customer_name' => $request->input('customer_name'),
            'customer_email' => $request->input('customer_email'),
            'customer_phone' => $request->input('customer_phone'),
            'product_name' => $product->name,
            'qty' => $request->input('qty'),
            'payment_type' => $request->input('payment_type'),
            'total_bayar' => $total,
            'total_bayar_perbulan' => $biaya_perbulan
        ];
        $this->response->setData($data, 'detail_transaksi');
        $this->response->setToken(Auth::user()->token);
        $this->response->setStatus(ApiResponse::SUCCESS);
        return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
    }

    public function createInstallment(Request $request)
    {
        // check sales data
        $sales = Sales::where('id', $request->input('sales_id'))->first();
        if (empty($sales)) {
            $this->response->setMessage('Data Transaksi tidak ditemukan.');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        // check payment data
        $payments = Payment::where('sales_id', $sales->id)->get();
        if (!empty($payments)) {
            $amountAlreadyPaid = 0;
            foreach ($payments as $payment) {
                $amountAlreadyPaid += $payment->amount;
            }

            if ($amountAlreadyPaid >= $sales->total) {
                $this->response->setMessage('Transaksi sudah lunas.');
                $this->response->setToken(Auth::user()->token);
                $this->response->setStatus(ApiResponse::ERR_INVALID_ARGUMENT);
                return response()->json($this->response->toArray(), HttpResponse::$HTTP_ERROR);
            }
        }

        // save payment
        $req_create_payment = [
            'sales_id' => $sales->id,
            'amount' => $request->input('amount'),
            'payment_method' => $request->input('payment_method'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];

        Payment::create($req_create_payment);

        $this->response->setData($req_create_payment, 'detail_transaksi');
        $this->response->setToken(Auth::user()->token);
        $this->response->setStatus(ApiResponse::SUCCESS);
        return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
    }

}

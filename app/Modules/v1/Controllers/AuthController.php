<?php

namespace App\Modules\v1\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helpers\Api\ApiResponse;
use Helpers\Api\HttpResponse;
use Mapping\UserMapping;

class AuthController extends Controller
{

    protected $response;

    /**
     * contruct function for abstract class Api
     */
    public function __construct()
    {
        $this->response = new ApiResponse;
        $this->userMappingg = new UserMapping();
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $email = $request->input("email");
        $password = $request->input("password");

        $user = User::where("email", $email)->where("status", User::STATUS_ACTIVE)->first();

        if (!$user) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('User tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        if (Hash::check($password, $user->password)) {
            $msg = [
                "user" => $this->userMappingg->mappingDetailUsers($user)
            ];

            $this->response->setData($msg);
            $this->response->setStatus(ApiResponse::SUCCESS);
            $this->response->setToken($user->token);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INVALID_LOGIN);
            $this->response->setMessage('Password salah');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_FORBIDDEN);
        }
    }
}

<?php

$router->group(
    [
        'prefix' => 'v1',
    ],
    function () use ($router) {
        $router->get('/healthcheck', function () use ($router) {
            return "Hello World!";
        });
        $router->post('/login', 'AuthController@login');

        $router->group(
            [
                'middleware' => 'auth',
            ],
            function () use ($router) {
                // transactions
                $router->post('transaction', 'TransactionsController@create');
                $router->post('transaction/installment', 'TransactionsController@createInstallment');
            });
    });

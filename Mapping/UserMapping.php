<?php namespace Mapping;

use App\Models\User;
use App\Models\UserBalances;
use Mapping\MappingReturn;

class UserMapping extends MappingReturn
{
    public function mappingDetailUsers($user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'status' => $this->mappingStatusUser($user->status),
            'status_type' => $user->status,
        ];
    }

    private function mappingStatusUser($status)
    {
        $resp = '';
        if ($status == User::STATUS_ACTIVE) {
            $resp = 'active';
        }
        if ($status == User::STATUS_INACTIVE) {
            $resp = 'inactive';
        }
        return $resp;
    }
}

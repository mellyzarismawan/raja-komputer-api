<?php namespace Mapping;

class MappingReturn
{
    public function __construct()
    {
    }
    /**
     * this function for mapping tax
     *
     * @param  object  $cashflow The cashflow
     * @return array
     */
    public function fieldMapping(
        $tax,
        $mapping_name
    ) {
        $mapping_result = $this->{$mapping_name};
        $result         = [];
        foreach ($this->{$mapping_name} as $key => $value) {
            if ($mapping_result[$value]) {
                $result[$key] = $tax->{$value};
            }
        }
        return $result;
    }
}

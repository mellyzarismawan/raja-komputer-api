<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $payment_type_enum = array('cash', 'debit', 'onlinepayment');

            $table->increments('id');
            $table->unsignedInteger('sales_id');
            $table->decimal('amount', 15, 2);
            $table->enum('payment_method', $payment_type_enum)->default('cash');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id', 'payments_migrations_id_idx');
            $table->index('sales_id', 'payments_migrations_sales_id_fk_idx');

            $table->foreign('sales_id', 'payments_migrations_sales_id_fk')->references('id')->on('sales')->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $payment_type_enum = array('cash', 'credit');

            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('credit_id')->nullable();
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('user_id');
            $table->integer('qty');
            $table->integer('bunga');
            $table->integer('lama');
            $table->decimal('total', 15, 2);
            $table->enum('payment_type', $payment_type_enum)->default('cash');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id', 'sales_migrations_id_idx');
            $table->index('product_id', 'sales_migrations_product_id_fk_idx');
            $table->index('customer_id', 'sales_migrations_customer_id_fk_idx');
            $table->index('user_id', 'sales_migrations_user_id_fk_idx');

            $table->foreign('product_id', 'sales_migrations_product_id_fk')->references('id')->on('products')->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->foreign('credit_id', 'sales_migrations_credit_id_fk')->references('id')->on('credits')->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->foreign('customer_id', 'sales_migrations_customer_id_fk')->references('id')->on('customers')->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->foreign('user_id', 'sales_migrations_user_id_fk')->references('id')->on('users')->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('customers', function (Blueprint $table) {
            $gender_enum = array("M", "F");

            $table->increments('id');
            $table->string('name', 255)->default('');
            $table->string('email', 60)->default('');
            $table->text('address');
            $table->string('phone', 50)->default('');
            $table->enum('gender', $gender_enum)->default('M');
            $table->string('identity_number', 255)->default('');
            $table->string('image', 255)->nullable();
            $table->tinyInteger('status')->comment('1: active, 2: inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

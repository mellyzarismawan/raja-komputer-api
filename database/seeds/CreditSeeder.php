<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CreditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credits')->insert([
            ['lama' => '6', 'bunga' => '12', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'Admin', 'email' => 'admin@admin.com', 'password' => Hash::make('123456'), 'status' => 1, 'token' => base64_encode(str_random(40)), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}
